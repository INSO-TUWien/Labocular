import requests as req
import json
import sys
import os

# set default URLs for the Minocular and Visocular project, can be overwritten in environment variables
DEFAULT_MINOCULAR_URL = 'https://gitlab.com/INSO-TUWien/Minocular.git'
DEFAULT_VISOCULAR_URL = 'https://gitlab.com/INSO-TUWien/Visocular.git'

minocular_url = os.environ['MINOCULAR_URL'] if 'MINOCULAR_URL' in os.environ and len(os.environ['MINOCULAR_URL']) > 0 else DEFAULT_MINOCULAR_URL
visocular_url = os.environ['VISOCULAR_URL'] if 'VISOCULAR_URL' in os.environ and len(os.environ['VISOCULAR_URL']) > 0 else DEFAULT_VISOCULAR_URL

# exit if GITLAB_TOKEN not set in environemnt variables
if 'ACCESS_TOKEN' not in os.environ:
  sys.exit('ACCESS_TOKEN environment variable has to be set!')

# token passed in manually, passed down to shadow projects
gitlab_token = os.environ['ACCESS_TOKEN'].strip()

# use GitLab token for authentication, needs to be set in environment variable
headers = {
  'PRIVATE-TOKEN': gitlab_token
}

# appends the given path to the base URL of the GitLab instance
def api_url(path):
  return os.environ['CI_API_V4_URL'] + '/' + path

# store the GitLab group (predefined GitLab CI/CD variables)
gitlab_group = os.environ['CI_PROJECT_NAMESPACE']

# get a list of all projects existing for the current group
all_projects = list(map(lambda x: x['name'], json.loads(req.get(api_url(f'groups/{gitlab_group}/projects'), headers=headers).text)))

if ('Minocular' in all_projects and 'Visocular' in all_projects):
  print(f'{gitlab_group}/Minocular and {gitlab_group}/Visocular project already exist');
  sys.exit(0);

# fetch group ID via API
gitlab_group_id = json.loads(req.get(api_url(f'groups/{gitlab_group}'), headers=headers).text)['id']

# create Minocular project under current namespace if it does not exist already
if ('Minocular' not in all_projects):
  res = req.post(api_url('projects'), data={
      'name': 'Minocular',
      'import_url': minocular_url,
      'namespace_id': gitlab_group_id
    }, headers=headers
  )
  print(res.text)
  print(f'Created repository: {gitlab_group}/Minocular')

# create Visocular project under current namespace
if ('Visocular' not in all_projects):
  res = req.post(api_url('projects'), data={
      'name': 'Visocular',
      'import_url': visocular_url,
      'namespace_id': gitlab_group_id
    }, headers=headers
  )
  print(res.text)
  print(f'Created repository: {gitlab_group}/Visocular')
