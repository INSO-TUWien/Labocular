import requests as req
import json
import os

# store the GitLab group (predefined GitLab CI/CD variables)
gitlab_group = os.environ['CI_PROJECT_NAMESPACE']

# token passed in manually, passed down to shadow projects
gitlab_token = os.environ['ACCESS_TOKEN']

# use GitLab token for authentication, needs to be set in environment variable
headers = {
  'PRIVATE-TOKEN': gitlab_token
}

# appends the given path to the base URL of the GitLab instance
def apiUrl(path):
  return os.environ['CI_API_V4_URL'] + '/' + path

# set default suffix for shadow projects, can be overwritten by setting environment variable
shadow_project_suffix = os.environ['SHADOW_PROJECT_SUFFIX'] if 'SHADOW_PROJECT_SUFFIX' in os.environ else 'shadow'

# fetch all projects for the current group
gitlab_group = os.environ['CI_PROJECT_NAMESPACE']
all_projects = json.loads(req.get(apiUrl(f'groups/{gitlab_group}/projects'), headers=headers).text)

# filter projects (either to the ones defined in environment variable or the ones matching the prefix)
filtered_projects = list(filter(lambda p: p['name'].endswith(shadow_project_suffix), all_projects))

if (len(filtered_projects) == 0):
  print('No shadow projects found')

for p in filtered_projects:
  # delete shadow project
  res = req.delete(apiUrl('projects/' + str(p['id'])), headers=headers)

  if res.ok:
    print('Deleted ' + p['path_with_namespace'])
  else:
    print(f"Error: {json.loads(res.text)['message']}")
