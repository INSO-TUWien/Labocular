# install Binocular Python CLI Application
pip install binocular-cli --no-cache-dir --index-url https://__token__:$ACCESS_TOKEN@reset.inso.tuwien.ac.at/repo/api/v4/projects/1716/packages/pypi/simple
echo "binocular-python installed"

# setting ENV Variables
export ACCESS_TOKEN=$ACCESS_TOKEN
export GITLAB_API_URL=$CI_SERVER_URL
export SCHEDULED_PIPELINE_DESC="Daily mine"

# writing config file
echo '{"description": "Daily mine","ref": "main","cron": "0 2 * * *","cron_timezone": "Europe/Vienna","active": true}' > schedule_config.json

# executing binocular cli python application
binocular scheduler create -g Binocular/$PROJECT_GROUP-$SHADOW_PROJECT_SUFFIX -template schedule_config.json
