FROM python:alpine3.13

COPY . .

RUN apk --no-cache add git
RUN pip install --no-cache-dir -r requirements.txt
