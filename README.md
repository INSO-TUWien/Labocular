# Labocular

Project used for the administration of an integrated software repository mining and visualisation solution. Once the Labocular project has been cloned ([https://gitlab.com/INSO-TUWien/Labocular](https://gitlab.com/INSO-TUWien/Labocular)), the `build_labocular_image` and `setup_required_projects` jobs have to be executed (in that order). Moreover, force pushes to the `main` branch have to be enabled in "Settings > Repository >  Protected branches". Lastly, setting the `GITLAB_TOKEN` variable (with `api` scope) in "Settings > CI/CD > Variables" is recommended, since it is required in multiple jobs.

### `build_labocular_image`

Builds the Docker image that is used to run the setup scripts in. Does not require any environment variables.

### `create_shadow_projects`

Creates the shadow projects for existing projects. The following environment variables are required, either by passing them manually when executing the pipeline or by setting them in the CI/CD settings:

* `GITLAB_TOKEN` [GitLab access token](https://gitlab.com/-/profile/personal_access_tokens), will be passed to shadow repositories (`api` scope is required, since a repository is created, cannot be seen by students, since they are added with the guest role)
* `SELECTED_REPOSITORIES` a comma separated list holding repository names, pipeline only creates shadow projects for these repos, or `PROJECT_PREFIX` the project prefix for student projects, pipeline creates shadow projects for all projects with this prefix

Either `PROJECT_PREFIX` or `SELECTED_REPOSITORIES` has to be set, otherwise pipeline does not know which shadow projects should be created. If both are set, `SELECTED_REPOSITORIES` takes precedence.

The `SHADOW_PROJECT_SUFFIX` variable is optional, and, if set, changes the suffix of shadow project repositories.

Note: variables values have to be lowercase!

### `setup_required_projects`

Clones the Minocular and Visocular repositories from [https://gitlab.com/INSO-TUWien](https://gitlab.com/INSO-TUWien). Requires the `GITLAB_TOKEN` with the `api` scope.

### `update_labocular_project`, `update_minocular_project` & `update_visocular_project`

Update the given project to the latest version. Also requires `GITLAB_TOKEN` with `api` scope to work.

Optional `COMMIT_HASH` environment variable can be set to upgrade/downgrade to the given commit.