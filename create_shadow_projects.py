import requests as req
import json
import sys
import os

# store predefined GitLab CI/CD variables
# gitlab_username = os.environ['GITLAB_USER_LOGIN']
gitlab_username = 'binocular-robot'
gitlab_group = os.environ['CI_PROJECT_NAMESPACE']
repository_group_lowercase = os.environ['PROJECT_GROUP']
ci_server_url = os.environ['CI_SERVER_URL']
gitlab_group_lowercase = gitlab_group.lower()

if repository_group_lowercase == "":
  repository_group_lowercase = gitlab_group.lower()
# token passed in manually, passed down to shadow projects

gitlab_token = os.environ['ACCESS_TOKEN'].strip()


# use GitLab token for authentication, needs to be set in environment variable
headers = {
  'PRIVATE-TOKEN': gitlab_token
}

# guest level is sufficient for viewing job artifacts
# https://docs.gitlab.com/ee/api/members.html#valid-access-levels
GITLAB_GUEST_ACCESS_LEVEL = 10

# path to the Minocular repository
MINOCULAR_REPO = f"https://{gitlab_username}:{gitlab_token}@{ci_server_url[8:]}/{gitlab_group}/Minocular.git"

# set default suffix for shadow projects, can be overwritten by setting environment variable
shadow_project_suffix = os.environ['SHADOW_PROJECT_SUFFIX'] if 'SHADOW_PROJECT_SUFFIX' in os.environ and len(os.environ['SHADOW_PROJECT_SUFFIX']) > 0 else 'shadow'

# appends the given path to the base URL of the GitLab instance
def api_url(path):
  return os.environ['CI_API_V4_URL'] + '/' + path

# returns the name of the shadow source_project for a given source_project
def shadow_repo_name(source_project_name):
  return f"{source_project_name}-{shadow_project_suffix}"


# exit if GITLAB_TOKEN not set in environemnt variables
if 'ACCESS_TOKEN' not in os.environ:
  sys.exit('ACCESS_TOKEN environment variable has to be set!')

# exit if both SELECTED_REPOSITORIES nor PROJECT_PREFIX environment variable are not set
if (('SELECTED_REPOSITORIES' not in os.environ or os.environ['SELECTED_REPOSITORIES'] == '')
    and ('PROJECT_PREFIX' not in os.environ or os.environ['PROJECT_PREFIX'] == '')):
  sys.exit(f"Either 'PROJECT_PREFIX' or 'SELECTED_REPOSITORIES' environment variable has to be set!")

# fetch all projects for the current user
page = 1
all_projects = []
while True:
    resp = req.get(api_url(f'groups/{repository_group_lowercase}/projects?page={page}'), headers=headers)
    page = resp.headers['x-next-page']
    all_projects.extend(json.loads(resp.text))
    if page == '':
        break

# filter projects (either to the ones defined in environment variable or the ones matching the prefix)
if 'SELECTED_REPOSITORIES' in os.environ and os.environ['SELECTED_REPOSITORIES'] != "":
  filtered_projects = list(filter(lambda p: p['name'] in os.environ['SELECTED_REPOSITORIES'].split(','), all_projects))
else:
  filtered_projects = list(filter(lambda p: p['name'].startswith(os.environ['PROJECT_PREFIX']), all_projects))


# exit if there are no projects
if (len(filtered_projects) == 0):
  print('No projects matching the criteria!')
  print('SELECTED_REPOSITORIES: ' + os.environ['SELECTED_REPOSITORIES'])
  print('PROJECT_PREFIX: ' + os.environ['PROJECT_PREFIX'])
  sys.exit()

# fetch group ID via API
gitlab_group_id = json.loads(req.get(api_url(f'groups/{gitlab_group}'), headers=headers).text)['id']

sub_group_name = repository_group_lowercase+'-'+os.environ['SHADOW_PROJECT_SUFFIX']

req.post(api_url(f'groups?parent_id={gitlab_group_id}'), data={
    'name': sub_group_name,
    'path': sub_group_name.lower(),
    'description': f'shadow group for {repository_group_lowercase} that contains the shadow projects for this group'
  }, headers=headers
)

gitlab_sub_group_id = json.loads(req.get(api_url(f'groups?search={sub_group_name}'), headers=headers).text)[0]['id']

# create shadow projects for all matching projects
for source_project in filtered_projects:
  # check if source_project already has shadow project
  source_project_name = source_project['name']

  if shadow_repo_name(source_project_name) in list(map(lambda p: p['name'], all_projects)):
    print(f"Shadow source_project for {source_project_name} already exists")
    continue

  # create shadow source_project and import Minocular
  res = req.post(api_url('projects'), data={
      'name': shadow_repo_name(source_project_name),
      'import_url': MINOCULAR_REPO,
      'namespace_id': gitlab_sub_group_id
    }, headers=headers
  )

  # store shadow repository object
  shadow_repo = json.loads(res.text)
  print(f"Created shadow repository: {shadow_repo['path_with_namespace']}")

  # fetch members from source project and add them as guests to shadow project
  members = json.loads(req.get(source_project['_links']['members'], headers=headers).text)

  #ADD: original project memeber as guest to shadow project
  #=====================================
  """
  for member in members:
    # current user created the project, thus does not need to be added
    if member['username'] == gitlab_group:
      continue

    # send POST request to members endpoint of shadow repository
    res = req.post(shadow_repo['_links']['members'], data = {
      'user_id': member['id'],
      'access_level': GITLAB_GUEST_ACCESS_LEVEL
    }, headers=headers)

    print(f"Added {member['username']} as guest")
  """
  #=====================================


  # add gitlab token, as well as host, name and username of source repository
  req.post(api_url(f"projects/{shadow_repo['id']}/variables"), data = {
    'key': 'GITLAB_TOKEN',
    'value': gitlab_token,
    'masked': True
  }, headers=headers)

  req.post(api_url(f"projects/{shadow_repo['id']}/variables"), data = {
    'key': 'REPO_HOST',
    'value': ci_server_url[8:],
  }, headers=headers)

  req.post(api_url(f"projects/{shadow_repo['id']}/variables"), data = {
    'key': 'REPO_USER',
    'value': source_project['namespace']['path'],
  }, headers=headers)

  req.post(api_url(f"projects/{shadow_repo['id']}/variables"), data = {
    'key': 'REPO_NAME',
    'value': source_project_name,
  }, headers=headers)

  req.post(api_url(f"projects/{shadow_repo['id']}/variables"), data = {
    'key': 'GROUP_NAME_LOWERCASE',
    'value': gitlab_group_lowercase,
  }, headers=headers)

  req.post(api_url(f"projects/{shadow_repo['id']}/variables"), data = {
    'key': 'GROUP_NAME_LOWERCASE',
    'value': gitlab_group_lowercase,
  }, headers=headers)

  req.post(api_url(f"projects/{shadow_repo['id']}/variables"), data = {
    'key': 'GITLAB_PROJECT_ID',
    'value': source_project['id'],
  }, headers=headers)

  print(f"Set variables GITLAB_TOKEN, REPO_HOST, REPO_USER, REPO_NAME for: {shadow_repo['name']}")
