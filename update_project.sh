if [[ -z $ACCESS_TOKEN ]]; then
  echo "ACCESS_TOKEN variable is not set!"
  exit 1
fi

if [[ -z $PROJECT ]]; then
  echo "PROJECT variable is not set!"
  exit 1
fi

# pull changes from source into target, optionally reset to given commit hash
TARGET_URL="https://$GITLAB_USER_LOGIN:$ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_NAMESPACE/$PROJECT.git"
SOURCE_URL="https://$GITLAB_USER_LOGIN:$ACCESS_TOKEN@gitlab.com/INSO-TUWien/$PROJECT.git"
BRANCH="main"

git clone $TARGET_URL $PROJECT
cd $PROJECT

git pull $SOURCE_URL $BRANCH

if [[ -n $COMMIT_HASH ]]; then
  echo "Resetting to commit: $COMMIT_HASH"
  git reset --hard $COMMIT_HASH
fi

echo "Pushing to $TARGET_URL ($BRANCH)"
git push $TARGET_URL $BRANCH --force
